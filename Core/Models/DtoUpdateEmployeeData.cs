﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DtoUpdateEmployeeData
    {
        [Required]
        public Guid? EmployeeID { get; set; }


        [Required, MaxLength(100, ErrorMessage = "You have exceeded the character length 100."), RegularExpression(@"^[\p{L} \.\-]+$")]
        public string? EmployeeName { get; set; }


        [Required, MaxLength(100, ErrorMessage = "You have exceeded the character length 100."), RegularExpression(@"^[\p{L} \.\-]+$")]
        public string? Department { get; set; }

        public Guid? DesignationID { get; set; }

        public double Salary { get; set; }


        [Required, MaxLength(100, ErrorMessage = "You have exceeded the character length 100."), RegularExpression(@"^[\p{L} \.\-]+$")]
        public string? City { get; set; }

       
        public DateTime UpdatedOn { get; set; }

        public Guid? UpdatedBy { get; set; }

    }
}
