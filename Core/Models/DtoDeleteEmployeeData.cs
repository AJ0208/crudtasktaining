﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Models
{
    public class DtoDeleteEmployeeData
    {
        [Required]
        public Guid? EmployeeID { get; set; }
        public DateTime DeletedOn { get; set; }
        public Guid? DeletedBy { get; set; }
    }
}
