﻿using Core.Interface;
using Core.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using static Core.Models.EmployeeListParameter;

namespace NewCrudTask.Controllers
{
    [Route("api/Employee")]

    public class EmployeeController : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        public EmployeeController(IEmployeeRepository employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        private async Task<ActionResult<T>> HandleInvalidModelState<T>(Func<Task<T>> action)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new
                {
                    Status = false,
                    Message = string.Join(Environment.NewLine, ModelState.Values
                                            .SelectMany(x => x.Errors)
                                            .Select(x => x.ErrorMessage)),

                });
            }

            T response = await action();

            if (response == null)
            {
                return NotFound();
            }

            return Ok(response);
        }

        private async Task<ActionResult<T>> HandleException<T>(Func<Task<T>> action)
        {
            try
            {
                return await HandleInvalidModelState(action);
            }
            catch (Exception ex)
            {
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        [HttpPost]
        public async Task<ActionResult<Response>> InsertData([FromBody] DtoAddEmployeeData dtoAddEmployeeData)
        {
            return await HandleException(async () => await _employeeRepository.AddAsync(dtoAddEmployeeData));
        }


        [HttpPut]
        public async Task<ActionResult<Response>> UpdateData([FromBody] DtoUpdateEmployeeData dtoUpdateEmployeeData)
        {
            return await HandleException(async () => await _employeeRepository.UpdateAsync(dtoUpdateEmployeeData));
        }

        [HttpDelete]
        public async Task<ActionResult<Response>> DeleteData([FromBody] DtoDeleteEmployeeData dtoDeleteEmployeeData)
        {
            return await HandleException(async () => await _employeeRepository.DeleteAsync(dtoDeleteEmployeeData));
        }

        [HttpGet, Route("GetAllEmployeesList")]
        public async Task<ActionResult<ResponseList>> GetAllData(EmployeeListParameter employeeListParameter)
        {
            return await HandleException(async () => await _employeeRepository.GetAllEmployeeListAsync(employeeListParameter));
            //return Ok(result);
        }


    }
}
