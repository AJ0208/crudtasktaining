﻿
using Core.Interface;
using Core.Models;
using Dapper;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static Core.Models.EmployeeListParameter;
using Response = Core.Models.Response;

namespace Infrastructure
{
    public class EmpoyeeRepository : IEmployeeRepository
    {
        public readonly IConfiguration _configuration;

        private static string? connectionString = string.Empty;

        public EmpoyeeRepository(IConfiguration configuration)
        {
            _configuration = configuration;
            connectionString = _configuration["ConnectionStrings:CrudTask"];
        }

        public static IDbConnection Connection
        {
            get
            {
                return new SqlConnection(connectionString);
            }
        }

        public async Task<Response> AddAsync(DtoAddEmployeeData dtoAddEmployeeData)
        {
            Response response;

            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspAddEmployeeData]", new
            {
                dtoAddEmployeeData.EmployeeName,
                dtoAddEmployeeData.Department,
                dtoAddEmployeeData.DesignationID,
                dtoAddEmployeeData.Salary,
                dtoAddEmployeeData.City,
                dtoAddEmployeeData.CreatedBy

            }, commandType: CommandType.StoredProcedure);

            return response;

        }

        public async Task<Response> UpdateAsync(DtoUpdateEmployeeData dtoUpdateEmployeeData)
        {
            Response response;
            using IDbConnection db = Connection;
            response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspUpdateEmployeeData]", new
            {
                EmployeeID = dtoUpdateEmployeeData.EmployeeID,
                EmployeeName = dtoUpdateEmployeeData.EmployeeName,
                Department = dtoUpdateEmployeeData.Department,
                DesignationID = dtoUpdateEmployeeData.DesignationID,
                Salary = dtoUpdateEmployeeData.Salary,
                City = dtoUpdateEmployeeData.City,
                dtoUpdateEmployeeData.UpdatedOn,
                dtoUpdateEmployeeData.UpdatedBy

            }, commandType: CommandType.StoredProcedure);
            return response;
        }

        public async Task<Response> DeleteAsync(DtoDeleteEmployeeData dtoDeleteEmployeeData)
        {         
                Response response;

                using IDbConnection db = Connection;
                response = await db.QueryFirstOrDefaultAsync<Response>("[dbo].[uspDeleteEmployeeData]", new
                {
                    EmployeeID = dtoDeleteEmployeeData.EmployeeID,
                    dtoDeleteEmployeeData.DeletedOn,
                    dtoDeleteEmployeeData.DeletedBy

                }, commandType: CommandType.StoredProcedure);
                return response;

            }

        public async Task<ResponseList> GetAllEmployeeListAsync(EmployeeListParameter employeeListParameter)
        {
            ResponseList response;
            using IDbConnection db = Connection;
            var result = await  db.QueryMultipleAsync("[uspGetAllEmployeesListByDesignation]", employeeListParameter, commandType: CommandType.StoredProcedure);


            response = result.Read<ResponseList>().FirstOrDefault()!;
            response.Data = result.Read<EmployeesList>().ToList();
            response.TotalRecords = result.Read<int>().FirstOrDefault();
            return response;


        }
           

    }


}




