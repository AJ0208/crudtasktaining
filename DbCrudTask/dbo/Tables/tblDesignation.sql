﻿CREATE TABLE [dbo].[tblDesignation] (
    [DesignationID]   UNIQUEIDENTIFIER CONSTRAINT [DF_Designation_DesignationID] DEFAULT (newid()) NOT NULL,
    [DesignationName] VARCHAR (100)    NOT NULL,
    CONSTRAINT [PK_Designation] PRIMARY KEY CLUSTERED ([DesignationID] ASC)
);

