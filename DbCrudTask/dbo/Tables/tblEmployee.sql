﻿CREATE TABLE [dbo].[tblEmployee] (
    [EmployeeID]    UNIQUEIDENTIFIER DEFAULT (newid()) NOT NULL,
    [EmployeeName]  VARCHAR (100)    NOT NULL,
    [Department]    VARCHAR (100)    NOT NULL,
    [DesignationID] UNIQUEIDENTIFIER NOT NULL,
    [Salary]        MONEY            NULL,
    [City]          VARCHAR (100)    NULL,
    [CreatedOn]     DATETIME         DEFAULT (getutcdate()) NOT NULL,
    [CreatedBy]     UNIQUEIDENTIFIER NULL,
    [UpdatedOn]     DATETIME         NULL,
    [UpdatedBy]     UNIQUEIDENTIFIER NULL,
    [DeletedOn]     DATETIME         NULL,
    [DeletedBy]     UNIQUEIDENTIFIER NULL,
    [Active]        BIT              DEFAULT ((1)) NOT NULL,
    CONSTRAINT [FK_DesignationID] FOREIGN KEY ([DesignationID]) REFERENCES [dbo].[tblDesignation] ([DesignationID])
);

