﻿CREATE PROCEDURE [uspGetAllEmployeesListByDesignation]
(
    @Start INT = 0,
    @PageSize INT = - 1,
    @SortCol VARCHAR(100) = NULL,
    @SearchKey VARCHAR(500) = '',
    @DesignationID VARCHAR(50) =null
)
AS
BEGIN
begin try
   
    SET NOCOUNT ON;

    SET @SortCol = TRIM(ISNULL(@SortCol, ''));
    SET @SearchKey = TRIM(ISNULL(@SearchKey, ''));
    IF ISNULL(@Start, 0) = 0 SET @Start = 0
    IF ISNULL(@PageSize, 0) <= 0 SET @PageSize = - 1
    SELECT 1 AS [Status],'Success' AS [Message]

    
    DECLARE @TotalRecords BIGINT
    SELECT EmployeeID , EmployeeName , Department , DesignationName  , Salary , City, CreatedOn , CreatedBy , UpdatedOn , UpdatedBy , DeletedOn , DeletedBy
    INTO #TEMP
    FROM [dbo].[tblEmployee] E WITH (NOLOCK)
    INNER JOIN [dbo].[tblDesignation] D
    ON E.[DesignationID] = D.[DesignationID]

    WHERE(EmployeeName LIKE '%'+ @SearchKey+'%'  )

    AND (ISNULL(@DesignationID,'')='' or E.DesignationID = @DesignationID)
    ORDER BY CASE WHEN @SortCol = 'EmployeeName_asc' THEN  EmployeeName END ASC,
             CASE WHEN @SortCol = 'EmployeeName_desc' THEN  EmployeeName END DESC  

    OFFSET @START ROW
    FETCH NEXT (CASE WHEN @PageSize = - 1 THEN (SELECT COUNT(1) FROM [dbo].[tblEmployee] [Total Records]) 
                                          ELSE  @PageSize END) ROWS ONLY

    SELECT * FROM #TEMP
    SELECT COUNT(*)
    FROM #TEMP

END TRY
BEGIN CATCH

        DECLARE @Msg VARCHAR(MAX) = Error_message();
        DECLARE @ErrorSeverity INT = Error_severity();
        DECLARE @ErrorState INT = Error_state();
        RAISERROR (@Msg,@ErrorSeverity ,@ErrorState );
        SELECT 0 AS [Status],@Msg AS [Message]

   END CATCH 
END