﻿CREATE PROCEDURE uspAddEmployeeData
(
      @EmployeeName VARCHAR(100),
	  @Department VARCHAR(100),
	  @DesignationID UNIQUEIDENTIFIER,
	  @Salary MONEY,
	  @City VARCHAR(100),
	  @CreatedBy UNIQUEIDENTIFIER
)	
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @EmployeeName = TRIM(@EmployeeName);
	SET @Department = TRIM(@Department);
	SET @City = TRIM(@City );

	IF NOT EXISTS (SELECT 1 FROM [dbo].[tblEmployee] WITH (NOLOCK) 
	WHERE EmployeeName = @EmployeeName AND ACTIVE = 1)

	   BEGIN

	        INSERT INTO [dbo].[tblEmployee]
	        (EmployeeName , Department , DesignationID , Salary , City , CreatedBy)
	        VALUES(TRIM(@EmployeeName) , TRIM(@Department) , @DesignationID , @Salary , TRIM(@City ) , @CreatedBy)

	        SELECT 'Employee details has been inserted successfully!' AS 'Message'
	   END

   Else

      BEGIN 
	       SELECT @EmployeeName + 'Employee already exists' AS 'Message'
	  END
END
