﻿CREATE PROCEDURE [dbo].[uspDeleteEmployeeData]
(
	@EmployeeID UNIQUEIDENTIFIER 
)
AS
BEGIN
	
	SET NOCOUNT ON;

	IF EXISTS (SELECT 1 FROM [dbo].[tblEmployee] WITH (NOLOCK) WHERE EmployeeID = @EmployeeID AND ACTIVE = 1)
	
	   BEGIN
    	      UPDATE [dbo].[tblEmployee]
	          SET 
			  Active = 0 ,
			  DeletedOn = GETUTCDATE() ,
			  DeletedBy = NEWID()
	          WHERE EmployeeID = @EmployeeID

	          SELECT 'Data have been deleted successfully!' AS 'Message'
	    END

	ELSE

	    BEGIN	
	          SELECT 'Given employee value does not exists please check and try again!' AS 'Message'
	    END
END
