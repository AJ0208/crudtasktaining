﻿-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================

CREATE PROCEDURE [dbo].[uspUpdateEmployeeData]
(
	@EmployeeID UNIQUEIDENTIFIER,
	@EmployeeName  VARCHAR(100),
	@Department  VARCHAR(100),
	@DesignationID  UNIQUEIDENTIFIER,
	@Salary MONEY,
	@City  VARCHAR(100)
)
AS
BEGIN
	
	SET NOCOUNT ON;

	SET @EmployeeName = TRIM(@EmployeeName);
	SET @Department = TRIM(@Department);
	SET @City = TRIM(@City );

	IF EXISTS (SELECT 1 FROM [dbo].[tblEmployee] WITH (NOLOCK) 
	WHERE EmployeeID = @EmployeeID AND Active = 1)

	     BEGIN
		       UPDATE [dbo].[tblEmployee]
			   SET
			   EmployeeName = TRIM(@EmployeeName),
			   Department  =  TRIM(@Department),
			   DesignationID = @DesignationID,
			   Salary = 	@Salary,
			   City  = TRIM(@City ) ,
			   UpdatedOn = GETUTCDATE(),
			   UpdatedBy = NEWID()
			   WHERE
			   EmployeeID  = @EmployeeID ;

			   
			  SELECT 'Employee data have been updated successfully' AS 'Message'

		  END
	ELSE
	      BEGIN
		       SELECT 'Employee does not exists please try again' AS 'Message'
		  END
 END
